const express 			= require('express');
const router 			= express.Router();
const UserController 	= require('../controllers/user.controller');
const passport      	= require('passport');
require('./../middleware/passport')(passport);

//routes
router.post(    '/users',                                                 UserController.create);
router.post(    '/users/login',                                           UserController.login);
router.get(     '/users', passport.authenticate('jwt',{session:false}),   UserController.get);
router.put(     '/users', passport.authenticate('jwt',{session:false}),   UserController.update);
router.delete(  '/users', passport.authenticate('jwt',{session:false}),   UserController.remove);

module.exports = router;